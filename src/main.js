import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import smoothscroll from 'smoothscroll-polyfill';
smoothscroll.polyfill();

import firebase from 'firebase/app';

Vue.config.productionTip = false

export const bus = new Vue();
// Vue.prototype.$bus = new Vue();

import loader from "vue-ui-preloader";
Vue.use(loader);

// import VueFreezeGif from 'vue-freeze-gif'
// import 'vue-freeze-gif/lib/vue-freeze-gif.css'
//
import VueFreezeframe from 'vue-freezeframe';
import "vue-cookie-accept-decline/dist/vue-cookie-accept-decline.css";
import VueCookieAcceptDecline from "vue-cookie-accept-decline";
Vue.component("vue-cookie-accept-decline", VueCookieAcceptDecline);

Vue.use(VueFreezeframe);

const firebaseConfig = {
  apiKey: "AIzaSyDp9IWtfUFso7KnES-oIiRpTNRDGOjuvY4",
  authDomain: "giffa-project.firebaseapp.com",
  projectId: "giffa-project",
  storageBucket: "giffa-project.appspot.com",
  messagingSenderId: "276186653444",
  appId: "1:276186653444:web:4966d30e459f1b7c5de651"
};
firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(user => {
  store.dispatch("fetchUser", user);
});

// console.log(firebase)

new Vue({
  router,
  store,
  vuetify,
  loader: loader,
  // firebase,
  render: h => h(App)
}).$mount('#app')
