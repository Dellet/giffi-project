import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);


export default new Vuetify({
    theme: {
        dark: true,
        options: {
            customProperties: true
        },
        themes: {
            light: {
                blocks_darkwhite: "#ffffff",
                blocks_darkwhite_header: "#f5f6fa",
                text_primary: colors.black,
            },
            dark: {
                blocks_darkwhite: "#1D1D1D",
                blocks_darkwhite_header: "#1D1D1D",
                text_primary: colors.white,
            },
        },

    },
});
