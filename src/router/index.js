import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from 'firebase';
import Dashboard from "../components/Dashboard";

import Join from "../views/Join";
import Profile from "../views/Profile";

import GifDetails from "../components/GifDetails";
import CommunityPage from "../components/CommunityPage";
import TrendsPage from "../components/TrendsPage";
import PopularPage from "../components/PopularPage";
import StickersPage from "../components/StickersPage";
import HelpPage from "../views/HelpPage";
import NotFound from "../components/NotFound";

import SearchResult from "../views/SearchResult";
// import store from "../store"
import NothingFound from "../views/NothingFound";


Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      props: true
    },
    {
      path: '/join',
      name: 'join',
      component: Join
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: {
        authRequired: true
      }
    },
    {
      path: '/result/:id',
      name: 'result',
      component: SearchResult,
      props: true
    },
    {
      path: '/details/:id',
      name: 'details',
      component: GifDetails,
      props: true
    },
    {
      path: '/community/:id',
      name: 'community',
      component: CommunityPage,
      props: true
    },
    {
      path: '/trends',
      name: 'trends',
      component: TrendsPage,
    },
    {
      path: '/popular',
      name: 'popular',
      component: PopularPage,
    },
    {
      path: '/stickers',
      name: 'stickers',
      component: StickersPage,
    },
    {
      path: '/help',
      name: 'help',
      component: HelpPage
    },
    {
      path: '/emptysearch',
      name: 'nofind',
      component: NothingFound
    },
    {
      path: '/404',
      name: 'NotFound',
      component: NotFound
    },
    {
      path: '*', redirect: '/404'
    },
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.authRequired)) {
    if (firebase.auth().currentUser) {
      next();
    } else {
      alert('Вы должны зарегестрироваться или войти');
      next({
        path: '/',
      });
    }
  } else {
    next();
  }
});

export default router;
